<?php
$arquivo = 'answer.json';
$alfabeto = array(
    1  =>'A',
    2  =>'B',
    3  =>'C',
    4  =>'D',
    5  =>'E',
    6  =>'F',
    7  =>'G',
    8  =>'H',
    9  =>'I',
    10  =>'J',
    11 =>'K',
    12 =>'L',
    13 =>'M',
    14 =>'N',
    15 =>'O',
    16 =>'P',
    17 =>'Q',
    18 =>'R',
    19 =>'S',
    20 =>'T',
    21 =>'U',
    22 =>'V',
    23 =>'W',
    24 =>'X',
    25 =>'Y',
    26 =>'Z'
);
$decifrado = '';
if(!file_exists($arquivo) || filesize($arquivo) == 0){
    $url = "https://api.codenation.dev/v1/challenge/dev-ps/generate-data?token=f15b96929f28185308a7c7ce2a3ba70a80922970";
    $curl = curl_init();
    curl_setopt($curl,CURLOPT_URL,$url);
    curl_setopt($curl, CURLOPT_HTTPGET, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $returno = curl_exec($curl);
    curl_close($curl);
    
    if(!$handle = fopen($arquivo,"w+")){
        echo "Erro ao tentar abrir o arquivo!";
        exit;
    }
    if(fwrite($handle,$returno) === FALSE){
        echo "Error ao escrever no arquivo";
        exit;
    }
    fclose ($handle);
}else{
    if(!$handle = fopen($arquivo,"r+")){
        echo "Erro ao tentar abrir o arquivo!";
        exit;
    }
    if(!$conteudo = fread($handle,filesize($arquivo))){
        echo "Error ao ler o arquivo";
        exit;
    }
    $objArquivo = json_decode($conteudo);
    // $tamanhostr = strlen('d oljhlud udsrvd pduurp vdowrx vreuh r fdfkruur fdqvdgr');
    $tamanhostr = strlen($objArquivo->cifrado);
    $str        = $objArquivo->cifrado;
   // $str        = 'd oljhlud udsrvd pduurp vdowrx vreuh r fdfkruur fdqvdgr';
    for($i = 0; $i < $tamanhostr;$i++ ){
        $keyAlfa = array_keys($alfabeto,strtoupper($str[$i]));
         if(!empty($keyAlfa)){
            // $k = $keyAlfa[0] - 3;
           if( $keyAlfa[0] > $objArquivo->numero_casas){
              $k = $keyAlfa[0] - $objArquivo->numero_casas;
            }else{
              $k = $objArquivo->numero_casas - $keyAlfa[0];
              $k = count($alfabeto) - $k;
            }

             $decifrado .= strtolower($alfabeto[$k]);
        }else{
            $decifrado .= $str[$i];
        }
    }
    $objArquivo->decifrado            = $decifrado;
    $objArquivo->resumo_criptografico = sha1($decifrado);
    // echo sha1($decifrado)."<br/>";
    // echo $objArquivo->cifrado; 
    //echo 'd oljhlud udsrvd pduurp vdowrx vreuh r fdfkruur fdqvdgr'; 
    $conteudo = json_encode($objArquivo);
    
    if(!$handle = fopen($arquivo,"w+")){
        echo "Erro ao tentar abrir o arquivo!";
        exit;
    }
    if(fwrite($handle,$conteudo) === FALSE){
        echo "Error ao escrever no arquivo";
        exit;
    }
    fclose ($handle);
        $url = "https://api.codenation.dev/v1/challenge/dev-ps/submit-solution?token=f15b96929f28185308a7c7ce2a3ba70a80922970";
        // $url = "http://localhost/testeaceleradev/upload.php";
        $cfile = curl_file_create($arquivo,'application/json','teste_name');
        $data = array('answer'=> $cfile);
        $curl = curl_init();
        // curl_setopt($curl, CURLOPT_HTTPHEADER,array('Content-Type: multipart/form-data'));
        curl_setopt($curl, CURLOPT_URL,$url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 0); 
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $returno = curl_exec($curl);
        $curl_error = curl_error($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        echo $returno;
        var_dump($status);
        var_dump($curl_error);


}
?>